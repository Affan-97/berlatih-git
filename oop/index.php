<?php
require_once('Animal.php');
require_once('Frog.php');
require_once('Ape.php');
$sheep = new Animal("shaun");

$sheep->get_name();
$sheep->get_legs();
$sheep->get_cold_blooded();
echo "<br>";
$kodok = new Frog("buduk");
$kodok->get_name();
$kodok->get_legs();
$kodok->get_cold_blooded();
$kodok->jump();

echo "<br>";
$sungokong = new Ape("kera sakti");
$sungokong->get_name();
$sungokong->get_legs();
$sungokong->get_cold_blooded();
$sungokong->yell();
