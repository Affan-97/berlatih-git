<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/table', function(){
   
    return view('content.table')->withName('Table')->withTitle('Table')->withBread('Table');
});
Route::get('/data-table', function(){
    return view('content.datatable')->withName('Data')->withTitle('Data Table')->withBread('Data Table');
});
Route::get('/dashboard', function(){
    return view('content.homedash')->withName('Home')->withTitle('Dashboard')->withBread('Dashboard');
});
Route::get('/cast', 'CastController@index');
Route::post('/cast', 'CastController@store');
Route::get('/cast/create', 'CastController@create');
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::get('/cast/{cast_id}', 'CastController@show');
Route::put('/cast/{cast_id}', 'CastController@update');
Route::delete('/cast/{cast_id}', 'CastController@destroy');

Route::get('/register', 'AuthController@register');
Route::post('/welcome', 'AuthController@welcome');
