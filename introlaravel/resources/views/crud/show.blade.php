@extends('template.template')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card" >
                <div class="card-body">
                  <h5 class="card-title">{{$cast->nama}}</h5><br>
                  <h6 class="text-muted"><b>Usia :</b> {{$cast->umur}}</h6>
                  <p class="card-text">{{$cast->bio}}</p>
                  <a href="/cast/{{$cast->id}}/edit" class="btn btn-warning">Edit</a>
                            <form action="/cast/{{$cast->id}}" method="post" class="d-inline">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>

                            <a href="/cast" class="btn btn-success">Kembali</a>
                </div>
              </div>
           
        </div>
    </div>
</div>
@endsection