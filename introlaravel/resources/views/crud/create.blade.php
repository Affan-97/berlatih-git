@extends('template/template')
@section('content')
<div class="container-fluid">
    <div class="w-50 m-auto bg-white p-3 rounded">
        <form action="/cast" method="POST">
            @csrf
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="nama">Name</label>
                <input type="text" class="form-control" id="nama" name="nama" placeholder="Name">
                @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
              </div>
              <div class="form-group col-md-6">
                <label for="umur">Umur</label>
                <input type="text" class="form-control"  name="umur" id="umur" placeholder="Umur">
                @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
              </div>
            </div>
            <div class="form-group">
                <label for="bio">Bio</label>
                <textarea class="form-control" placeholder="Description About You" id="bio" name="bio" rows="3"></textarea>
                @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
              </div>
            
            <button type="submit" class="btn btn-primary w-100">Tambah Data</button>
        </form>
    </div>
</div>

@endsection
