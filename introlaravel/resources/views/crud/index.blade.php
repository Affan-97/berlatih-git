@extends('template.template')
@section('content')
<div class="container-fluid">
    <a href="/cast/create" class="btn btn-primary mb-3">Tambah Data</a>
    <div class="card direct-chat direct-chat-primary">
        
        <div class="card-body">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Umur</th>
                        <th scope="col">Bio</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($cast as $key=>$value)
                 
                        <tr>
                            <th scope="row">{{$key+1}}</th>
                          
                            <td>{{$value->nama}}</td>
                            <td>{{$value->umur}}</td>
                            <td>{{$value->bio}}</td>
                            <td>
                                <a href="/cast/{{$value->id}}" class="btn btn-success"><i class="fas fa-eye"></i></a>
                                <a href="/cast/{{$value->id}}/edit" class="btn btn-primary"><i class="far fa-edit"></i></a>
                                <form action="/cast/{{$value->id}}" method="post" class="d-inline">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger my-1" ><i class="fa fa-trash"></i></button>
                                </form>
                               </td>
                            
                        </tr>
                        
                        @empty
                        <tr colspan="3">
                            <td>No data</td>
                        </tr>  
                    @endforelse  
                </tbody>
            </table>
        </div>
       
      
      </div>
</div>
@endsection