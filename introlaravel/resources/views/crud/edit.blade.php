@extends('template/template')
@section('content')
<div class="container-fluid">
    <div class="w-50 m-auto bg-white p-3 rounded">
        <form action="/cast/{{$cast->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="nama">Name</label>
                <input type="text" class="form-control" id="nama" name="nama" placeholder="Name" value="{{$cast->nama}}" >
                @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
              </div>
              <div class="form-group col-md-6">
                <label for="umur">Umur</label>
                <input type="text" class="form-control"  name="umur" id="umur" value="{{$cast->umur}}" placeholder="Umur">
                @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
              </div>
            </div>
            <div class="form-group">
                <label for="bio">Bio</label>
                <textarea class="form-control" id="bio" name="bio" value=""  rows="3">{{$cast->bio}}</textarea>
                @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
              </div>
            
            <button type="submit" class="btn btn-primary w-100">Update</button>
        </form>
    </div>
</div>

@endsection
