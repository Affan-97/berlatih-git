<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>

<body>

    <body>
        <div>
            <h1>SanberBook</h1>
            <div>
                <h2>Sign Up Form</h2>
                <form action="/welcome" method="post">
                    @csrf {{ csrf_field() }}
                    <label> First Name:</label><br>
                    <input type="text" name="fname" id="fname"><br><br>
                    <label> Last Name:</label><br>
                    <input type="text" name="lname" id="lname"><br><br>
                    <label> Gender: </label><br>
                    <input type="radio">Male<br>
                    <input type="radio">Female<br>
                    <input type="radio">Other<br><br>
                    <label> Nationality :</label><br>
                    <select name="nation">
                        <option value="indonesia">Indonesia</option>
                        <option value="singapore">Singapore</option>
                        <option value="malaysia">Malaysia</option>
                        <option value="philipines">Philipines</option>


                    </select><br><br>

                    <label> Language Spoken :</label><br>
                    <input type="checkbox">Bahasa Indonesia<br>
                    <input type="checkbox">English<br>
                    <input type="checkbox">Other<br><br>
                    <label> Bio:</label><br>
                    <textarea name="bio" cols="20" rows="5"></textarea><br><br>
                    <button type="submit">Submit</button>
                </form>
            </div>
        </div>

    </body>

</html>