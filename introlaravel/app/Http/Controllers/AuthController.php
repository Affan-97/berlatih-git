<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function welcome(Request $request)
    {
        $fname = $request->input('fname');
        $lname = $request->input('lname');



        return view('welcome', ['fname' => $fname, 'lname' => $lname]);
    }

    public function register()
    {
        return view('register');
    }
}
